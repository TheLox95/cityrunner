﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControladorCarro : MonoBehaviour {

	public float velocidad = 0.02f;
	public ControlVirtual botonIzquierda;
	public ControlVirtual botonDerecha;

	void Awake(){
		if(gameObject.tag == "Player" && SceneManager.GetActiveScene().buildIndex == 1){
			#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_WEBGL
				GameObject.Find("FechaDerecha").SetActive(false);
				GameObject.Find("FlechaIzquierda").SetActive(false);
			#endif
		}
	}

	void FixedUpdate () {
		Vector3 pos = transform.position;

		if(SceneManager.GetActiveScene().buildIndex == 1){
		#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_WEBGL
			if (Input.GetAxis ("Horizontal") > 0) {
			pos.x += velocidad;
			
			}else if (Input.GetAxis ("Horizontal") < 0){
				pos.x -= velocidad;
			}

		#else
			
			if(gameObject.tag != "Destruible"){
				if(botonIzquierda.pulsado == true){
					pos.x -= velocidad;
				}else if(botonDerecha.pulsado == true){
					pos.x += velocidad;
				}
			}
		#endif
		}
		
		pos.y += velocidad;
		transform.position = pos;
	}
}
