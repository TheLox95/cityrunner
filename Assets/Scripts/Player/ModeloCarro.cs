﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ModeloCarro : MonoBehaviour {

	public MainIA gm;
	public GameObject gameOver;
	private ControladorCarro carro;
	public Puntaje puntaje;

	void Start(){
		carro = gameObject.GetComponent<ControladorCarro> ();
	}

	IEnumerator OnCollisionEnter2D(Collision2D col){
		if (gm.controlEnable == true) {
			if (col.collider.CompareTag ("Destruible") || col.collider.CompareTag ("Ground")) {
				
				carro.velocidad = 0.003f;
				Time.timeScale = 0.2f;
				yield return new WaitForSeconds (0.25f);
				gameOver.SetActive (true);
				Time.timeScale = 0;
				puntaje.EstablecerPuntaje ();
			}
		}
	}

}
