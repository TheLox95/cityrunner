﻿using UnityEngine;
using System.Collections;

public class CamaraSeguir : MonoBehaviour {

	public Transform carro;

	void Start(){
		carro = GameObject.FindGameObjectWithTag ("FocusPoint").transform;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		transform.position = new Vector3 (carro.position.x, carro.position.y, carro.position.z);
	}
}
