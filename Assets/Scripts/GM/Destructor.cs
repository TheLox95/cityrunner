﻿using UnityEngine;
using System.Collections;

public class Destructor : MonoBehaviour {

	void OnTriggerExit2D(Collider2D col){
		if(col.CompareTag("Destruible")){
			Debug.Log(col.gameObject.tag);
			Destroy (col.gameObject);
		}
	}
}
