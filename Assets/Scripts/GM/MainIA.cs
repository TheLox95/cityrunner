﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainIA : MonoBehaviour {

	public bool controlEnable = true;
	public Puntaje puntaje;

	void Update(){
		if (Input.GetKeyDown(KeyCode.Escape)) {
    		Application.Quit(); 
		}
	}

	public void PlayGame(){
		SceneManager.LoadScene (1);
	}

	public void VolverJugar(){
		puntaje.Resetear ();
		SceneManager.LoadScene (0);
	}
}
