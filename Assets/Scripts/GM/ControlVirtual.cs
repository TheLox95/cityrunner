﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ControlVirtual : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	public bool pulsado = false;


	public void OnPointerDown(PointerEventData e){
		pulsado = true;
	}

	public void OnPointerUp(PointerEventData e){
		pulsado = false;
	}
}
