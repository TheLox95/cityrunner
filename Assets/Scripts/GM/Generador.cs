﻿using UnityEngine;
using System.Collections;

public class Generador : MonoBehaviour {

	public GameObject[] objs;
	public float tiempoMin = 1f;
	public float tiempoMax = 2f;
	private ControladorCarro carro;
	private int velocidad = 100;

	// Use this for initialization
	void Awake () {
		Generar ();
	}


	void Generar(){
		Instantiate (objs[Random.Range(0,objs.Length)], transform.position, Quaternion.identity);
		Invoke ("Generar", 68f);
	}
}
