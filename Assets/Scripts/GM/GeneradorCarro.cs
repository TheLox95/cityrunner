﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GeneradorCarro : MonoBehaviour {

	public GameObject obj;

	// Use this for initialization
	void Start () {
		Generar ();
	}


	void Generar(){
		if(SceneManager.GetActiveScene().buildIndex == 1){
			Instantiate (obj, new Vector3 (Random.Range (-1.3f, 0.9f), transform.position.y, transform.position.z), Quaternion.identity);

			Invoke ("Generar", 0.5f);
		}
	}
}
