﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;
using System.Collections.Generic;
using System;
using UnityEngine.UI;


public class FBMainIA : MonoBehaviour {

	public Text textoRecord;
	public Button botonIniciarSesionEnFacebook;
	public Button botonCompartirPuntaje;


	void Awake(){
		if(!FB.IsInitialized){
        // Initialize the Facebook SDK
			FB.Init(InitCallback, OnHideUnity);
		}
		else{
			// Already initialized, signal an app activation App Event
			FB.ActivateApp();
		}
    }	
    
    private void InitCallback(){
		if(FB.IsInitialized){
			// Signal an app activation App Event
			FB.ActivateApp();
			// Continue with Facebook SDK

			FBlogin();
		}
		else{
			Debug.Log("Failed to Initialize the Facebook SDK");
		}
	}
    
    private void OnHideUnity(bool isGameShown){
		if(!isGameShown){
			Time.timeScale = 0;
		}
		else{
			Time.timeScale = 1;
		}
	}
    
    
    public void FBlogin(){
		var perms = new List<string>(){"public_profile", "email", "user_friends","publish_actions"};
		FB.LogInWithPublishPermissions(perms, AuthCallback);
	}

	void AuthCallback(IResult resultado){
		if(resultado.Error != null){
			Debug.Log(resultado.Error);
		}else{
			if(FB.IsLoggedIn){
				botonCompartirPuntaje.gameObject.SetActive(false);
				botonIniciarSesionEnFacebook.gameObject.SetActive(true);

				Debug.Log("Facebook esta logueado");
				// NO CARGAR EL RECORD INMEDIATAMENTE DESPUES DE LOGUEARME!!!
				//FB.API("/me/scores", HttpMethod.GET, LoadScoreCallback);

				botonIniciarSesionEnFacebook.gameObject.SetActive(false);
				botonCompartirPuntaje.gameObject.SetActive(true);
			}else{
				Debug.Log("Facebook no esta logueado");
			}
		}
	}

	public void UpdateScore(IGraphResult result){
		FB.API("/me/scores", HttpMethod.GET, LoadScoreCallback);
	}

	public void LoadScoreCallback(IGraphResult result){		
		var dataList = result.ResultDictionary["data"] as List<object>;
		int recordLocal = ((int)PlayerPrefs.GetFloat ("Record"));
		string recordEnString = recordLocal.ToString();

		if(dataList.Count == 0){
			Debug.Log("Asignando score por primera vez");
			AssingScore();
		}
		else{
			var dataDict = dataList[0] as Dictionary<string, object>;

			if(Int32.Parse(dataDict["score"].ToString()) > recordLocal){
				//Record en facebook es mayor que el almacenado localmente
				recordEnString = dataDict["score"].ToString();
				PlayerPrefs.SetFloat ("Record", float.Parse(dataDict["score"].ToString()));
			}
    	}

    	AssingScore();
			
    	textoRecord.text = recordEnString;
		    	
	}

	public void AssingScore(){
		if(FB.IsInitialized){
			var scoreData = new Dictionary<string, string>() {{"score", ((int)PlayerPrefs.GetFloat ("Record")).ToString()}};
			FB.API("/me/scores", HttpMethod.POST, UpdateScore, scoreData);
		}
	}

	public void ShareScoreOnFacebook(){
		FB.API("/me/scores", HttpMethod.GET, CreateShareWindow);
	}

	void CreateShareWindow(IGraphResult result){
		var dataList = result.ResultDictionary["data"] as List<object>;
		var userObj = dataList[0] as Dictionary<string, object>;
		foreach(KeyValuePair<string, object>llave in userObj){
			Debug.Log(llave.Key + llave.Value);
		}
		var userInfo = userObj["user"] as Dictionary<string, object>;
		if(FB.IsInitialized){
			FB.FeedShare(
				link: new Uri("https://apps.facebook.com/1548087868835112"),
				linkName: "Nuevo Record en City Runner!",
				linkCaption: "¿Crees que puedes superarlo? Intentalo!",
				linkDescription: userInfo["name"] + " obtuvo un Nuevo Record en City Runner de " + userObj["score"].ToString() + " segundos!",
				callback: ShareCallback
			);
		}else{
			Debug.Log("facebook no esta inicializado...");
		}		
	}

	private void ShareCallback (IShareResult result) {
	    if (result.Cancelled || !String.IsNullOrEmpty(result.Error)) {
	        Debug.Log("ShareLink Error: "+result.Error);
	    } else if (!String.IsNullOrEmpty(result.PostId)) {
	        // Print post identifier of the shared content
	        Debug.Log(result.PostId);
	    } else {
	        // Share succeeded without postID
	        Debug.Log("Exito link compartido");
	    }
	}

	public void ActiveShareOption(){
		botonCompartirPuntaje.gameObject.SetActive(true);
	}
}
