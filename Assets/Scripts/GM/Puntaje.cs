﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Facebook.Unity;
using System;

public class Puntaje : MonoBehaviour {

	private float tiempo = 0.00f;
	private float tiempoObtenido;
	public Text textoPuntajeActual;
	public Text nuevoPuntaje;
	public Text textoRecord;
	public MainIA gm;
	public FBMainIA FBIA;

	
	// Update is called once per frame
	void Update () {
		if (gm.controlEnable == true) {
			tiempo = (float)Math.Round (Time.timeSinceLevelLoad,2);
			tiempoObtenido = tiempo;
			textoPuntajeActual.text = tiempo.ToString();
		}
	}

	public void Resetear(){
		Time.timeScale = 1;
	}

	public void EstablecerPuntaje(){
		nuevoPuntaje.text = ("Tiempo: ") + textoPuntajeActual.text;

		if(PlayerPrefs.HasKey("Record")){
			if (PlayerPrefs.GetFloat ("Record") < tiempoObtenido) {
				PlayerPrefs.SetFloat ("Record", tiempoObtenido);
			}
		}else{
			PlayerPrefs.SetFloat ("Record", tiempoObtenido);
		}

 		FBIA.ActiveShareOption();
 		FBIA.AssingScore();
	}

	
}
